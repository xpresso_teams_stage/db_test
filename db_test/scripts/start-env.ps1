#########################
# This script is for reference. Not being used
# This script manages the xpresso environment locally.
########################

$global:USERNAME="admin"
$global:PASSWORD="Abz00ba@123"

$global:DOCKER_HOSTNAME="xpresso"
$global:DOCKER_NAME="xpresso_devenv"
$global:DOCKER_REGISTRY="dockerregistry.xpresso.ai"
$global:DOCKER_IMAGE="$global:DOCKER_REGISTRY/library/xpresso_local:2.1.5"
$global:APP_FOLDER="/app"

$global:mounts=""
$global:command=""

$global:base_project_name="db_test"
if($global:base_project_name -ne "db_test" ){
  $global:project_name=$global:base_project_name
}
else{
  $global:project_name=""
}

$global:base_component_name="db_test"
if($global:base_component_name -ne "db_test"){
  $global:component_name=$global:base_component_name
}
else{
  $global:component_name=""
}

$global:base_image=$global:DOCKER_IMAGE
$global:container_name=$global:DOCKER_NAME

Function init
{
    $global:container_name=$global:DOCKER_NAME
    $global:xpresso_host_prefix="xpresso"
    $global:image_name="$global:DOCKER_REGISTRY/xprops/local/xpresso_local_env:latest"
	  $global:xpresso_host_name="$global:xpresso_host_prefix"
    if($global:project_name -ne ""){
      $global:container_name="$global:container_name-$global:project_name"
      $global:xpresso_host_name="$global:xpresso_host_prefix-$global:project_name"
      $global:image_name="$global:DOCKER_REGISTRY/xprops/$global:project_name/base/xpresso_local_env:latest"
    }
    if($global:component_name -ne ""){
      $global:container_name="$global:project_name--$global:component_name"
      $global:xpresso_host_name="$global:xpresso_host_prefix_$global:project_name_$global:component_name"
      $global:image_name="$global:DOCKER_REGISTRY/xprops/$global:project_name/base/$global:component_name/xpresso_local_env:latest"
    }

    if($global:base_image -eq ""){
      base_image=$global:DOCKER_IMAGE
    }

    Write-Host "Container name: $global:container_name"
    Write-Host "Base image: $global:base_image"
    Write-Host "Updated image name: $global:image_name"
    Write-Host "Xpresso host name: $global:xpresso_host_name"

}

Function push
{
  # Login to docker
  Invoke-Expression -Command "docker login $global:DOCKER_REGISTRY -u admin -p Abz00ba@123"
  Write-Host "Pushing your image: [$global:image_name] to the xpresso registry"
  Invoke-Expression -Command "docker push $global:image_name"
}

Function start_environment
{
    Write-Host "Validating the workspace"
    # Login to docker
    Invoke-Expression -Command "docker login $global:DOCKER_REGISTRY -u admin -p Abz00ba@123"
    Invoke-Expression -Command "docker pull $base_image"
	  $cmd = "docker tag $base_image $image_name"
    Invoke-Expression -Command $cmd
	
    $global:options="s"
    $OutputVariable = docker ps -a -f "name=$global:container_name"
    if($OutputVariable -ne ""){
      $global:options = Read-Host -Prompt "Previous container is still running. Do you want to exec into it/exit existing and start new container/ignore?[e/s/i]: "
      if($global:options -eq "s"){
        Invoke-Expression -Command "docker rm -f $global:container_name"
      }elseif($global:options -eq "e"){
        $OutputVariable = "docker container inspect -f '{{.State.Running}}' $global:container_name"
        if($OutputVariable -eq "true"){
          Invoke-Expression -Command "docker exec -it $container_name"
        }else{
          Write-Host "Error: Docker container is not running. Please use option 's'. Exiting..."
          exit 0
        }
      }else{
        Write-Host "Nothing to do now. Exiting"
        exit 0
       }
    }

    if($global:options -eq "s"){
      Write-Host "Initiating the workspace"	  
	  $global:curr_dir = ${PSScriptRoot}.Split(':')
      if($global:mounts -eq ""){
		$cmd="docker run -it --hostname=$global:xpresso_host_name  --privileged -e PROJECT_NAME=$global:project_name -e COMPONENT_NAME=$global:component_name --name=$global:container_name --net=host   -v //var/run/docker.sock:/var/run/docker.sock   -v  '$global:curr_dir:$global:APP_FOLDER'  $global:image_name"
		Write-Host $cmd
        Invoke-Expression -Command  $cmd
      }else{
        $cmd="docker run -it --hostname=$global:xpresso_host_name --privileged -e PROJECT_NAME=$global:project_name -e COMPONENT_NAME=$global:component_name --name=$global:container_name --net=host   -v //var/run/docker.sock:/var/run/docker.sock   -v  '$global:curr_dir:$global:APP_FOLDER'  -v $global:mounts $global:image_name"
        Invoke-Expression -Command  $cmd
      }
    }

    $yesorno = Read-Host -Prompt "Do you want to persist the changes?[y/n]: "
    if($yesorno -eq "y"){
		Write-Host "Persisting your workspace"
		$cmd="docker commit $global:container_name $global:image_name"
		Invoke-Expression -Command  $cmd
		Write-Host ""
		Write-Host "Workspace has been saved here --> $global:image_name"
		Write-Host ""
		Write-Host "Note: You can now use this as your base image in your Dockerfile"
    }else{
		Write-Host "Nothing to do now. Exiting"
		
	}
  exit 0
}


init
start_environment
exit 0
